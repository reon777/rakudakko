export default {
  strict: true,
  namespaced: true,
  state: {
    uuid: '',
    nickname: '',
    self_introduction: '',
    pref: ''
  },
  mutations: {
    set_uuid(state, uuid) {
      state.uuid = uuid
    },
    set_nickname(state, nickname) {
      state.nickname = nickname
    },
    set_self_introduction(state, self_introduction) {
      state.self_introduction = self_introduction
    },
    set_pref(state, pref) {
      state.pref = pref
    }
  }
}
