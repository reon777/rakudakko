import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import './registerServiceWorker'

// FontAwesomeIcon
import '@fortawesome/fontawesome-free-webfonts/css/fontawesome.css'
import '@fortawesome/fontawesome-free-webfonts/css/fa-brands.css'
import '@fortawesome/fontawesome-free-webfonts/css/fa-regular.css'
import '@fortawesome/fontawesome-free-webfonts/css/fa-solid.css'

// ローディング
import 'vue-loading-overlay/dist/vue-loading.css'
import Loading from 'vue-loading-overlay'
Vue.use(Loading)
Vue.component('loading', Loading)

// firebase
import firebase from 'firebase'
import { firebaseConfig } from './../config/firebase-config'
firebase.initializeApp(firebaseConfig)

// CSSフレームワーク
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
Vue.use(Vuetify)

// 評価
import StarRating from 'vue-star-rating'
Vue.component('star-rating', StarRating)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
