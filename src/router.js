import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/companyDetail',
      name: 'companyDetail',
      component: () => import('./views/CompanyDetail.vue')
    },
    {
      path: '/driveAreaList',
      name: 'driveAreaList',
      component: () => import('./views/DriveAreaList.vue')
    },
    {
      path: '/companies',
      name: 'companies',
      component: () => import('./views/Companies.vue')
    },
    {
      path: '/company_edit',
      name: 'company_edit',
      component: () => import('./views/CompanyEdit.vue')
    },
    {
      path: '/myPage',
      name: 'myPage',
      component: () => import('./views/MyPage.vue')
    },
    {
      path: '/registration',
      name: 'registration',
      component: () => import('./views/Registration.vue')
    },
    {
      path: '/writeReview',
      name: 'writeReview',
      component: () => import('./views/WriteReview.vue')
    },
    {
      path: '/favorites',
      name: 'favorites',
      component: () => import('./views/Favorites.vue')
    },
    {
      path: '/ReviewDatail',
      name: 'ReviewDatail',
      component: () => import('./views/ReviewDatail.vue')
    },
    {
      path: '/CallHistory',
      name: 'CallHistory',
      component: () => import('./views/CallHistory.vue')
    },
    {
      path: '/SearchResult',
      name: 'SearchResult',
      component: () => import('./views/SearchResult.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth) {
    // このルートはログインされているかどうか認証が必要です。
    // もしされていないならば、ログインページにリダイレクトします。
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        next()
      } else {
        alert('ログインが必要です！')
        next({
          path: '/registration',
          query: { redirect: to.fullPath }
        })
      }
    })
  } else {
    next() // next() を常に呼び出すようにしてください!
  }
})

export default router
