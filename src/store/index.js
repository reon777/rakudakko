import Vue from 'vue'
import Vuex from 'vuex'
import persistedstate from 'vuex-persistedstate'
import breadcrumbs from './modules/breadcrumbs'
import user from './modules/user'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    breadcrumbs,
    user
  },
  state: {
    area: '',
    area_all: [],
    companies: [],
    company: {},
    review: {},
    search_word: ''
  },
  mutations: {
    set_area(state, area) {
      state.area = area
    },
    set_area_all(state, area_all) {
      state.area_all = area_all
    },
    set_companies(state, companies) {
      state.companies = companies
    },
    delete_companies(state) {
      state.companies = []
    },
    set_company(state, company) {
      state.company = company
    },
    set_review(state, review) {
      state.review = review
    },
    set_search_word(state, search_word) {
      state.search_word = search_word
    }
  },
  plugins: [persistedstate()]
})
