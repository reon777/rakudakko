export default {
  strict: true,
  namespaced: true,
  state: {
    breadcrumbs_list: []
  },
  mutations: {
    push(state, data) {
      state.breadcrumbs_list.push(data)
    },
    reset(state) {
      state.breadcrumbs_list = []
    }
  }
}
