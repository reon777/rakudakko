/* eslint-disable no-console */

import { register } from 'register-service-worker'

register(`${process.env.BASE_URL}service-worker.js`, {
  ready() {
    console.log(
      'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
    )
  },
  registered() {
    console.log('Service worker has been registered.')
  },
  cached() {
    console.log('Content has been cached for offline use.')
  },
  updatefound() {
    console.log('New content is downloading.')
    // iosは効かない。androidは未確認
    console.log('[register]caches: ' + JSON.stringify(caches))
    caches.keys().then(keys => {
      console.log('[register]keys: ' + keys)
      return Promise.all(
        keys.map(key => {
          console.log('不要なキャッシュを削除')
          return caches.delete(key)
        })
      )
    })
  },
  updated() {
    console.log('New content is available; please refresh.')
  },
  offline() {
    console.log('No internet connection found. App is running in offline mode.')
  },
  error(error) {
    console.error('Error during service worker registration:', error)
  }
})
