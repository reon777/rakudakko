# 概要

運転代行検索アプリ
https://rakudakko-377bc.firebaseapp.com

# 機能一覧

- 地域一覧を表示
- 地域ごとの会社一覧を表示
- 会社詳細を表示
- 電話をかける
- お気に入り登録・削除
- レビュー登録
- プロフィール登録
- 検索

# 利用技術

- フロントエンド
  - フレームワーク
    　 Vue.js 3.0
- サーバ
  Firebase
- DB
  FireStore
- ホスティング
  Firebase Hosting
- 検索
  Algolia

## 事前準備

- `src/config_sample.js`にAlgoliaのapiKeyを記載する
- ファイル名を変更する：`src/config_sample.js`→`src/config.js`

# 使い方

```bash
# ライブラリインストール
yarn install

# ブラウザ開発
yarn run serve

# ビルド
yarn run build

# ユニットテスト
yarn test:unit
```

## デプロイ（自動化しているのでプッシュすると以下が実行される）

Firebase Hosting
https://console.firebase.google.com/project/rakudakko-377bc/hosting/main?hl=ja

```bash
npm install -g firebase-tools
firebase login
yarn run build
firebase init
firebase deploy

```

アプリ URL
https://rakudakko-377bc.firebaseapp.com/

firebase deploy --only functions
