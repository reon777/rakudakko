const functions = require('firebase-functions')
const admin = require('firebase-admin')
admin.initializeApp(functions.config().firebase)
const algoliasearch = require('algoliasearch')

const ALGOLIA_ID = functions.config().algolia.app_id
const ALGOLIA_ADMIN_KEY = functions.config().algolia.api_key
const ALGOLIA_INDEX_NAME = functions.config().algolia.index_name

const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY)
// コレクションに新規レコードが追加されると起動
exports.onProductUpdated = functions.firestore
  .document('companies/{id}')
  .onCreate((snap, context) => {
    // 新規レコードの情報をIndex用オブジェクトに格納
    const data = snap.data()
    // objectIDをIndexするオブジェクトに追加する
    data.objectID = context.params.id
    // AlgoliaへIndex
    const index = client.initIndex(ALGOLIA_INDEX_NAME)
    return index.saveObject(data)
  })
